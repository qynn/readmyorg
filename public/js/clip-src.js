
$(function() {
    // console.log( "document ready!" );
    new ClipboardJS('.clip');

    // [copy] links for file blocks
    $('.src-file').each(function(index){
        var ref = 'srcf'+ index;
        $(this).attr('id', ref);

        var l = '<a style="font-size: 80%;" class="clip" href="javascript:void(0)" data-clipboard-target="#'+ref+'">[copy]</a>';
        $(this).before(' ' + l);
    });


    // source blocks w/ coderefs
    $('span[id^="coderef"]').each(function(){

        var coderef = $(this).attr("id");
        // console.log(coderef);

        var ref = coderef.substring(8,coderef.length);
        // console.log(ref);

        if (ref == "com"){ // comments
            $(this).css("color", "#cc99ff");
        }
        else{ // $ and # links
            $(this).css("font-weight", 700);
            var l1 = '<a class="clip" href="javascript:void(0)" data-clipboard-target="#'+coderef+'">'
            var l2 = '</a>'

            if( $(this).parent().hasClass("src-user") ){
                l2 = '$' + l2 + ' ';
            }
            else if( $(this).parent().hasClass("src-root") ){
                l2 = '#' + l2 + ' ';
            }
            else if( $(this).parent().hasClass("src-chroot") ){
                l1 = '(chroot) ' + l1;
                l2 = '#' + l2 + ' ';
            }



            $(this).before(l1+l2);
        }


    });

});
