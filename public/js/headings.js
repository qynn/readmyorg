
$(function() {

    // skip line before headers
    $('[class^=section-number-]').before('<br>');
    // remove numbers for sub-subsections (e.g. 1.3.2)
    $('.section-number-4').remove();
    // remove empty line before lists
    $('ul').prev().css('margin-bottom', 0);
    // remove empty line before source blocks
    $('.src').parent().prev().css('margin-bottom', 12);
});
